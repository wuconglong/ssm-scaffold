<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
<head>
  <meta charset="utf-8">
  <link rel="stylesheet" href="<%=request.getContextPath()%>/static/layuiadmin/layui/css/layui.css" media="all">
  <script src="<%=request.getContextPath()%>/static/js/jquery-3.2.1.min.js"></script>
  <script src="<%=request.getContextPath()%>/static/js/common.js"></script>
  <script src="<%=request.getContextPath()%>/static/layuiadmin/layui/layui.js"></script>
</head>
<body>
<div class="layui-fluid">
	<div class="layui-row layui-col-space15">
		<div class="layui-col-md12">
			<div class="layui-card">
				<div class="layui-card-header">设置我的资料</div>
				<div class="layui-card-body" pad15>
					<div class="layui-form" lay-filter="">
<div class="layui-form-item"><div class="layui-input-inline"><input type="hidden" name="primaryKey" value="${anyOne.primaryKey}"></div></div>
<div class="layui-form-item"><label class="layui-form-label">用户名</label><div class="layui-input-inline"><input type="text" name="userName" disabled="disabled" lay-verify="required" class="layui-input" value="${anyOne.userName}"></div></div>
<div class="layui-form-item"><label class="layui-form-label">密码</label><div class="layui-input-inline"><input type="text" name="password" lay-verify="required" class="layui-input" value="${anyOne.password}"></div></div>
<div class="layui-form-item"><label class="layui-form-label">昵称</label><div class="layui-input-inline"><input type="text" name="nickName" lay-verify="required" class="layui-input" value="${anyOne.nickName}"></div></div>
<div class="layui-form-item"><label class="layui-form-label">身份证</label><div class="layui-input-inline"><input type="text" name="identityCards" lay-verify="identity" class="layui-input" value="${anyOne.identityCards}"></div></div>
<div class="layui-form-item"><label class="layui-form-label">生日</label><div class="layui-input-inline"><input type="text" name="birthDay"class="layui-input" id="birthDay" placeholder="yyyy-MM-dd HH:mm:ss"></div></div>
<div class="layui-form-item"><label class="layui-form-label">电话</label><div class="layui-input-inline"><input type="text" name="telephone" lay-verify="phone" class="layui-input" value="${anyOne.telephone}"></div></div>
<div class="layui-form-item"><label class="layui-form-label">邮箱</label><div class="layui-input-inline"><input type="text" name="mailbox" lay-verify="email" class="layui-input" value="${anyOne.mailbox}"></div></div>
<div class="layui-form-item"><label class="layui-form-label">性别</label><div class="layui-input-inline"><input type="radio" name="gender" value="1"/>男<input type="radio" name="gender" value="0"/>女</div></div>
<div class="layui-form-item"><label class="layui-form-label">车牌号</label><div class="layui-input-inline"><input type="text" name="licensePlateNumber" lay-verify="required" class="layui-input" value="${anyOne.licensePlateNumber}"></div></div>
<div class="layui-form-item"><div class="layui-input-block"><button class="layui-btn" lay-submit lay-filter="layuiadmin-app-form-submit">确认修改</button></div></div>
					</div>
				</div>
			</div>
		</div>
	</div>
</div>
<script>
var gender = "${anyOne.gender}";
$("input[name='gender'][value='"+gender+"']").attr("checked","checked");
var birthDay = "${anyOne.birthDay}";
$("input[name='birthDay']").val(new Date(birthDay).format("yyyy-MM-dd hh:mm:ss"));
layui.use(['form', 'layedit', 'laydate','upload'], function(){
	  var form = layui.form,layer = layui.layer;
	  var laydate = layui.laydate;
	  
	//日期时间选择器
	  laydate.render({
	    elem: '#birthDay'
	    ,type: 'datetime'
	  });
	 
});
layui.config({base: '<%=request.getContextPath()%>/static/layuiadmin/'}).extend({index: 'lib/index'}).use(['index', 'form'], function(){
	var $ = layui.$
	var form = layui.form;
    //监听提交
    form.on('submit(layuiadmin-app-form-submit)', function(data){
    	var field = data.field;
	  	$.ajax({
	  		type:"post",
	  		dataType:'json',
	  		url:"<%=request.getContextPath()%>/updateSelectiveUser",
	  		data:field,
	  		success:function(result){
	  				layer.msg(result.msg);
	  		}
	      });
    });
  })
  </script>
</body>
</html>