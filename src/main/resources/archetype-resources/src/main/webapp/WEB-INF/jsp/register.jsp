<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
<head>
<meta charset="utf-8">
<title></title>
<meta name="renderer" content="webkit">
<meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">
<meta name="viewport" content="width=device-width, initial-scale=1.0, minimum-scale=1.0, maximum-scale=1.0, user-scalable=0">
<link rel="stylesheet" href="<%=request.getContextPath()%>/static/layuiadmin/layui/css/layui.css" media="all">
<link rel="stylesheet" href="<%=request.getContextPath()%>/static/layuiadmin/style/admin.css" media="all">
<link rel="stylesheet" href="<%=request.getContextPath()%>/static/layuiadmin/style/login.css" media="all">
</head>
<body>
	<div class="layadmin-user-login layadmin-user-display-show" id="LAY-user-login" style="display: none;">
		<div class="layadmin-user-login-main">
			<div class="layadmin-user-login-box layadmin-user-login-header">
				<h2>账号注册
				</h2>
				<p></p>
			</div>
			<div class="layadmin-user-login-box layadmin-user-login-body layui-form">
				<div class="layui-form-item">
					<label class="layadmin-user-login-icon layui-icon layui-icon-username" for="LAY-user-login-username"></label> <input type="text" name="userName" id="LAY-user-login-username"
						lay-verify="userName" placeholder="用户名" class="layui-input">
				</div>
				<div class="layui-form-item">
					<label class="layadmin-user-login-icon layui-icon layui-icon-password" for="LAY-user-login-password"></label> <input type="password" name="password" id="LAY-user-login-password" lay-verify="pass"
						placeholder="密码" class="layui-input">
				</div>
				<div class="layui-form-item">
					<label class="layadmin-user-login-icon layui-icon layui-icon-password" for="LAY-user-login-repass"></label> <input type="password" name="repass" id="LAY-user-login-repass" lay-verify="required"
						placeholder="确认密码" class="layui-input">
				</div>
				<div class="layui-form-item">
					<label class="layadmin-user-login-icon layui-icon layui-icon-username" for="LAY-user-login-nickname"></label><input type="text" name="nickName" id="LAY-user-login-nickname" lay-verify="nickname"
						placeholder="昵称" class="layui-input">
				</div>
				<div class="layui-form-item">
					<button class="layui-btn layui-btn-fluid" lay-submit lay-filter="LAY-user-reg-submit">注 册</button>
				</div>
				<div class="layui-trans layui-form-item layadmin-user-login-other">
					<a href="<%=request.getContextPath()%>/toLogin" class="layadmin-user-jump-change layadmin-link">登入</a>
				</div>
			</div>
		</div>

	</div>

	<script src="<%=request.getContextPath()%>/static/layuiadmin/layui/layui.js"></script>
	<script>
  layui.config({
    base: '<%=request.getContextPath()%>/static/layuiadmin/' //静态资源所在路径
		}).extend({
			index : 'lib/index' //主入口模块
		}).use([ 'index', 'user' ],function() {
			var $ = layui.$, setter = layui.setter, admin = layui.admin, form = layui.form, router = layui.router();
			form.render();
			//提交
			form.on('submit(LAY-user-reg-submit)',function(obj) {
				var field = obj.field;
				//确认密码
				if (field.password !== field.repass) {
					return layer.msg('两次密码输入不一致');
				}
				$.ajax({
					type:'get',
					dataType:'json',
					url:'<%=request.getContextPath()%>/register',
					data:field,
					success:function(result){
						if (result.code == 0) {
							location.href = "<%=request.getContextPath()%>/";
						} else {
							layer.msg('注册失败');
						}
					}
				})
			return false;
		});
	});
	</script>
</body>
</html>