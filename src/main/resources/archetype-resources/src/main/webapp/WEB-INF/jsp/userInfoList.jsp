<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
<head>
<meta charset="utf-8">
<link rel="stylesheet" href="<%=request.getContextPath()%>/static/layuiadmin/layui/css/layui.css" media="all">
<link rel="stylesheet" href="<%=request.getContextPath()%>/static/layuiadmin/style/admin.css" media="all">
<script src="<%=request.getContextPath()%>/static/js/jquery-3.2.1.min.js"></script>
<script src="<%=request.getContextPath()%>/static/js/common.js"></script>
</head>
<body>
<div class="layui-fluid">
	<div class="layui-card">
		
		<div class="layui-form layui-card-header layuiadmin-card-header-auto">
			<div class="layui-form-item">
				<div class="layui-inline"><label class="layui-form-label">用户名</label><div class="layui-input-inline"><input type="text" name="userName" placeholder="请输入" autocomplete="off" class="layui-input"></div></div>
				<div class="layui-inline"><label class="layui-form-label">昵称中含</label><div class="layui-input-inline"><input type="text" name="nickName" placeholder="请输入" autocomplete="off" class="layui-input"></div></div>
				<div class="layui-inline"><label class="layui-form-label">性别</label><div class="layui-input-inline"><select name="gender"><option value="">不分性别</option><option value="0">女</option><option value="1">男</option></select></div></div>
				<div class="layui-inline"><button class="layui-btn layuiadmin-btn-list" lay-submit lay-filter="LAY-app-contlist-search"><i class="layui-icon layui-icon-search layuiadmin-button-btn"></i></button></div>
			</div>
		</div>
		
		<div class="layui-card-body">
			<div>
				<button class="layui-btn layuiadmin-btn-list" data-type="batchdelete">批量删除</button>
			</div>
			<table id="LAY-app-content-list" lay-filter="LAY-app-content-list"></table> 
		</div>
	</div>
</div>

  <script src="<%=request.getContextPath()%>/static/layuiadmin/layui/layui.js"></script>  
<script>
layui.config({base: '<%=request.getContextPath()%>/static/layuiadmin/'}).extend({index: 'lib/index'}).use(['index', 'table'], function(){
	var table = layui.table;
	var form = layui.form;
	form.on('submit(LAY-app-contlist-search)', function(data){
		table.reload('LAY-app-content-list', {where: data.field});
	});
	var $ = layui.$;
	var active = {
		insert: function(){
			layerOpen("添加用户","<%=request.getContextPath()%>/selectByPrimaryKey",5*90,table,layer);
		},
		update:function(){
			updateOne("编辑用户","<%=request.getContextPath()%>/selectByPrimaryKey",5*90,table,layer);
		},
		batchdelete: function(){
			deleteByExample("<%=request.getContextPath()%>/deleteByExample","post",table,layer);
		}
	};
    $('.layui-btn.layuiadmin-btn-list').on('click', function(){
      var type = $(this).data('type');
      active[type] ? active[type].call(this) : '';
	});
    table.render({
        elem: "#LAY-app-content-list",
        url: "<%=request.getContextPath()%>/selectUsersJoin",
        cols: [[
        	{field: "primaryKey",type: "checkbox",fixed: "left"},
        	{field: "userName",minWidth: 80,title: "用户名"},
        	{field: "nickName",minWidth: 80,title: "昵称"},
        	{field: "roleInfo",minWidth: 80,title: "角色"},
        	{field: "identityCards",minWidth: 80,title: "身份证"},
        	{field: "birthDay",minWidth: 200,title: "生日",toolbar: "#birthDay"},
        	{field: "telephone",minWidth: 80,title: "电话"},
        	{field: "mailbox",minWidth: 200,title: "邮箱"},
        	{field: "gender",minWidth: 80,title: "性别",toolbar:"#gender"},
        	{field: "licensePlateNumber",minWidth: 80,title: "车牌号"}
        ]],
        page: !0,
        limit: 10,
        limits: [10, 15, 20, 25, 30]
    })
});
</script>
<script type="text/html" id="gender">
	{{ d.gender?'男':'女' }}
</script>
<script type="text/html" id="birthDay">{{ new Date(d.birthDay).format("yyyy-MM-dd hh:mm:ss") }}</script>
<script>
      	selectAJax("<%=request.getContextPath()%>/selectRoles","post","roleInfo");
      	</script>
</body>
</html>
