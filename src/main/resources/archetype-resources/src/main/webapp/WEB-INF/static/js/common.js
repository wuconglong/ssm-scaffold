function selectAJax(url,type,selectId){
	$.ajax({
		url:url,
		type:type,
		contentType:"text/json",
		async:false,
		cache:false,
		success:function(result){
			var length = result.length;
			for (var i = 0; i < length; i++) {
				var option = $("<option></option>");
				option.attr("value",result[i].primaryKey);
				option.text(result[i].displayValue);
				$("#"+selectId).append(option);
			}
		}
	});
}

function deleteByExample(url,type,table,layer){
	var checkStatus = table.checkStatus('LAY-app-content-list')
    ,checkData = checkStatus.data;
	if(checkData.length === 0){
		return layer.msg('请至少选择一条数据');
	}
	var a=new Array();
    for (var i = 0; i < checkData.length; i++) {
		a[i] = checkData[i].primaryKey;
	}
    layer.confirm('确定删除吗？', function(index) {
    	$.ajax({
    		type:type,
    		headers: {
    	        'Accept': 'application/json',
    	        'Content-Type': 'application/json'
    	    },
    		dataType:'json',
    		url:url,
    		data:JSON.stringify(a),
    		success:function(result){
    			layer.msg(result+'条记录已删除');
    			table.reload('LAY-app-content-list');
    		}
    	})
    });
}

function layerOpen(title,content,height,table,layer){
	layer.open({
		type:2,
		title:title,
		content:content,
		maxmin:true,
		area:['500px', height+'px'],
		btn: ['确定', '取消'],
		yes: function(index, layero){
			var submit = layero.find('iframe').contents().find("#layuiadmin-app-form-submit");
			submit.click();
			table.reload('LAY-app-content-list');
		}
	});
}

function updateOne(title,content,height,table,layer){
	var checkStatus = table.checkStatus('LAY-app-content-list')
	,checkData = checkStatus.data;
	if(checkData.length != 1){
		return layer.msg('请只选择一条数据');
	}
	layerOpen(title,content+"?primaryKey="+checkData[0].primaryKey,height,table,layer);
}

function submitOne(data,type,url){
	var field = data.field;
	var url = "";
	var action = "";
	if (field.primaryKey && field.primaryKey != "") {
		url = url+"updateByPrimaryKeySelective";
		action = "编辑";
	} else {
		url = url+"insertSelective";
		action = "添加";
	}
	$.ajax({
		type:type,
		dataType:'json',
		url:url,
		data:field,
		success:function(result){
			if (result != 0) {
				layer.msg(action+'成功');
			}
		}
    });
}

function tableNameConvert(tableName){
	var array = tableName.split("_");
	var result = "";
	for (var i = 0; i < array.length; i++) {
		result += firstLetterUppercase(array[i]);
	}
	return result;
}

function fieldNameConvert(fieldName){
	var array = fieldName.split("_");
	var result = firstLetterLowercase(array[0]);
	for (var i = 1; i < array.length; i++) {
		result += firstLetterUppercase(array[i]);
	}
	return result;
}

function firstLetterLowercase(word){
	return word.substring(0, 1).toLowerCase()+word.substring(1, word.length);
}

function firstLetterUppercase(word){
	return word.substring(0, 1).toUpperCase()+word.substring(1, word.length);
}
Date.prototype.format = function(format) {
	var o = {
		"M+" : this.getMonth() + 1,
		"d+" : this.getDate(),
		"h+" : this.getHours(),
		"m+" : this.getMinutes(),
		"s+" : this.getSeconds(),
		"q+" : Math.floor((this.getMonth() + 3) / 3),
		"S" : this.getMilliseconds()
	}
	if (/(y+)/.test(format)) {
		format = format.replace(RegExp.$1, (this.getFullYear() + "")
				.substr(4 - RegExp.$1.length));
	}
	for ( var k in o) {
		if (new RegExp("(" + k + ")").test(format)) {
			format = format.replace(RegExp.$1, RegExp.$1.length == 1 ? o[k]
					: ("00" + o[k]).substr(("" + o[k]).length));
		}
	}
	return format;
}