<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
<head>
<meta charset="utf-8">
<title></title>
<meta name="renderer" content="webkit">
<meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">
<meta name="viewport" content="width=device-width, initial-scale=1.0, minimum-scale=1.0, maximum-scale=1.0, user-scalable=0">
<link rel="stylesheet" href="<%=request.getContextPath()%>/static/layuiadmin/layui/css/layui.css" media="all">
<link rel="stylesheet" href="<%=request.getContextPath()%>/static/layuiadmin/style/admin.css" media="all">
<script src="<%=request.getContextPath()%>/static/layuiadmin/layui/layui.js"></script>
<script src="<%=request.getContextPath()%>/static/js/jquery-3.2.1.min.js"></script>
</head>
<body class="layui-layout-body">
	<div id="LAY_app">
		<div class="layui-layout layui-layout-admin">
		
			<!-- 顶部导航 -->
			<div class="layui-header">
				<ul class="layui-nav layui-layout-left">
					<li class="layui-nav-item layadmin-flexible" lay-unselect><a href="javascript:;" layadmin-event="flexible" title="侧边伸缩"><i class="layui-icon layui-icon-shrink-right" id="LAY_app_flexible"></i></a></li>
					<li class="layui-nav-item" lay-unselect><a href="javascript:;" layadmin-event="refresh" title="刷新"><i class="layui-icon layui-icon-refresh-3"></i></a></li>
				</ul>
				<ul class="layui-nav layui-layout-right" lay-filter="layadmin-layout-right">
					<li class="layui-nav-item layui-hide-xs" lay-unselect><a href="javascript:;" layadmin-event="fullscreen"><i class="layui-icon layui-icon-screen-full"></i></a></li>
					<li class="layui-nav-item" lay-unselect><a href="javascript:;"><cite>${sessionScope.USER_INFO.nickName}</cite></a>
						<dl class="layui-nav-child"><dd><a lay-href="selectUser">基本资料</a></dd><hr/><dd style="text-align: center;"><a href="<%=request.getContextPath()%>/logout">退出</a></dd></dl>
					</li>
				</ul>
			</div>
			<!-- 侧边菜单 -->
			<div class="layui-side layui-side-menu">
				<div class="layui-side-scroll">
					<script type="text/html" template="" lay-url="<%=request.getContextPath()%>/selectMenus" lay-done="layui.element.render('nav', 'layadmin-system-side-menu');" id="TPL_layout">

							<div class="layui-logo" lay-href="">
								<span>{{ layui.setter.name || 'layuiAdmin' }}</span>
							</div>

							<ul class="layui-nav layui-nav-tree" lay-shrink="all" id="LAY-system-side-menu" lay-filter="layadmin-system-side-menu">
								{{# var path = layui.router().path ,pathURL = layui.admin.correctRouter(path.join('/')) ,dataName = layui.setter.response.dataName; layui.each(d[dataName], function(index, item){ var hasChildren = typeof item.list === 'object' && item.list.length > 0 ,classSelected = function(){ var match = path[0] == item.menuName || (index == 0 && !path[0]) || (item.jump && pathURL == layui.admin.correctRouter(item.jump)) || item.spread; if(match){ return hasChildren ? 'layui-nav-itemed' : 'layui-this'; } return ''; } ,url = (item.jump && typeof item.jump === 'string') ? item.jump : item.node; }}
								<li data-name="{{ item.node || '' }}" data-jump="{{ item.jump || '' }}" class="layui-nav-item">
									<a href="javascript:;" {{ hasChildren ? '' : 'lay-href="'+ url + '"' }} lay-tips="{{ item.menuName }}" lay-direction="2">
										<i class="layui-icon {{ item.icon }}"></i>
										<cite>{{ item.menuName }}</cite>
									</a>
									{{# if(hasChildren){ }}
									<dl class="layui-nav-child">
										{{# layui.each(item.list, function(index2, item2){ var hasChildren2 = typeof item2.list == 'object' && item2.list.length > 0 ,classSelected2 = function(){ var match = (path[0] == item.node && path[1] == item2.node) || (item2.jump && pathURL == layui.admin.correctRouter(item2.jump)) || item2.spread; if(match){ return hasChildren2 ? 'layui-nav-itemed' : 'layui-this'; } return ''; } ,url2 = (item2.jump && typeof item2.jump === 'string') ? item2.jump : [item.node, item2.node, ''].join('/'); }}
										<dd data-name="{{ item2.node || '' }}" data-jump="{{ item2.jump || '' }}" {{ classSelected2() ? ( 'class="'+ classSelected2() + '"') : '' }}>
											<a href="javascript:;" {{ hasChildren2 ? '' : 'lay-href="'+ url2 + '"' }}>{{ item2.menuName }}</a>
											{{# if(hasChildren2){ }}
											<dl class="layui-nav-child">
												{{# layui.each(item2.list, function(index3, item3){ var match = (path[0] == item.node && path[1] == item2.node && path[2] == item3.node) || (item3.jump && pathURL == layui.admin.correctRouter(item3.jump)) ,url3 = (item3.jump && typeof item3.jump === 'string') ? item3.jump : [item.node, item2.node, item3.node].join('/') }}
												<dd data-name="{{ item3.name || '' }}" data-jump="{{ item3.jump || '' }}" {{ match ? 'class="layui-this"' : '' }}>
													<a href="javascript:;" lay-href="{{ url3 }}" {{ item3.iframe ? 'lay-iframe="true"' : '' }}>{{ item3.menuName }}</a>
												</dd>
												{{# }); }}
											</dl>
											{{# } }}
										</dd>
										{{# }); }}
									</dl>
									{{# } }}
								</li>
								{{# }); }}
							</ul>
						</script>
				</div>
			</div>

			<!-- 页面标签 -->
			<div class="layadmin-pagetabs" id="LAY_app_tabs">
				<div class="layui-icon layadmin-tabs-control layui-icon-prev" layadmin-event="leftPage"></div>
				<div class="layui-icon layadmin-tabs-control layui-icon-next" layadmin-event="rightPage"></div>
				<div class="layui-icon layadmin-tabs-control layui-icon-down">
					<ul class="layui-nav layadmin-tabs-select" lay-filter="layadmin-pagetabs-nav">
						<li class="layui-nav-item" lay-unselect><a href="javascript:;"></a>
							<dl class="layui-nav-child layui-anim-fadein">
								<dd layadmin-event="closeThisTabs">
									<a href="javascript:;">关闭当前标签页</a>
								</dd>
								<dd layadmin-event="closeOtherTabs">
									<a href="javascript:;">关闭其它标签页</a>
								</dd>
								<dd layadmin-event="closeAllTabs">
									<a href="javascript:;">关闭全部标签页</a>
								</dd>
							</dl></li>
					</ul>
				</div>
				<div class="layui-tab" lay-unauto lay-allowClose="true" lay-filter="layadmin-layout-tabs">
					<ul class="layui-tab-title" id="LAY_app_tabsheader">
						<li lay-id="<%=request.getContextPath()%>/console" lay-attr="<%=request.getContextPath()%>/console" class="layui-this"><i class="layui-icon layui-icon-home"></i></li>
					</ul>
				</div>
			</div>
			<!-- 主体内容 -->
			<div class="layui-body" id="LAY_app_body">
				<div class="layadmin-tabsbody-item layui-show">
					<iframe src="<%=request.getContextPath()%>/console" frameborder="0" class="layadmin-iframe"></iframe>
				</div>
			</div>
			<!-- 辅助元素，一般用于移动设备下遮罩 -->
			<div class="layadmin-body-shade" layadmin-event="shade"></div>
		</div>
	</div>
	<script>
	$("#headPortrait").click(function (){
		$("#headPortraitButton").click();
	})
  layui.config({
    base: '<%=request.getContextPath()%>/static/layuiadmin/' //静态资源所在路径
		}).extend({
			index : 'lib/index' //主入口模块
		}).use(['index','upload'],function(){
			var upload = layui.upload;
			var $ = layui.$;
			 upload.render({
			    elem: '#headPortraitButton'
			    ,url: '<%=request.getContextPath()%>/upload/',
			        before: function(obj){
			        },
			        done: function(res, index, upload){
			        	console.log("headPortrait");
				    	console.log(res);
				    	if (res.code == 0) {
				    		layer.msg('头像成功');
							$("#headPortraitButton").attr("src","<%=request.getContextPath()%>/static/upload/"+res.msg);
						} else {
							layer.msg('头像失败，上传的图片名称为'+res.msg);
						}
			        }
			  });
		});
	</script>
</body>
</html>