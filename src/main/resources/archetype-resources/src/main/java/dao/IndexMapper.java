package ${groupId}.dao;

import java.util.List;

import org.apache.ibatis.annotations.DeleteProvider;
import org.apache.ibatis.annotations.InsertProvider;
import org.apache.ibatis.annotations.SelectProvider;
import org.apache.ibatis.annotations.UpdateProvider;

import ${groupId}.pojo.MenuInfo;
import ${groupId}.pojo.RoleInfo;
import ${groupId}.pojo.UserInfo;

public interface IndexMapper {
	
	@SelectProvider(type = IndexProvider.class,method="countUsers")
	public long countUsers(UserInfo userInfo);
	
	@DeleteProvider(type = IndexProvider.class,method="deleteUser")
	public int deleteUser(UserInfo userInfo);
	
	@SelectProvider(type = IndexProvider.class,method="login")
	public UserInfo login(UserInfo userInfo);
	
	@InsertProvider(type = IndexProvider.class,method="register")
	public int register(UserInfo userInfo);
	
	@SelectProvider(type = IndexProvider.class,method="selectUsers")
	public List<UserInfo> selectUsers(UserInfo userInfo);
	
	@SelectProvider(type = IndexProvider.class,method="selectUsersJoin")
	public List<UserInfo> selectUsersJoin(UserInfo userInfo);
	
	@SelectProvider(type = IndexProvider.class,method="selectUser")
	public UserInfo selectUser(UserInfo userInfo);
	
	@UpdateProvider(type = IndexProvider.class,method="updateSelectiveUser")
	public int updateSelectiveUser(UserInfo userInfo);
	
	@SelectProvider(type = IndexProvider.class,method="selectRoles")
	public List<RoleInfo> selectRoles(RoleInfo roleInfo);
	
	@SelectProvider(type = IndexProvider.class,method="selectMenus")
	public List<MenuInfo> selectMenus(MenuInfo menuInfo);
	

}
