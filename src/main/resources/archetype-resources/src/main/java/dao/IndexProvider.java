package ${groupId}.dao;

import org.apache.ibatis.jdbc.SQL;

import cn.hutool.core.util.StrUtil;
import ${groupId}.pojo.MenuInfo;
import ${groupId}.pojo.RoleInfo;
import ${groupId}.pojo.UserInfo;

public class IndexProvider {
	
	public String countUsers(UserInfo userInfo) {
		String sql = new SQL() {{
			SELECT("COUNT(primary_key)");
			FROM("user_info");
			if (StrUtil.isNotEmpty(userInfo.getPrimaryKey())) {
				WHERE("primary_key <> #{primaryKey}");
			}
			if (StrUtil.isNotEmpty(userInfo.getUserName())) {
				WHERE("user_name = #{userName}");
			}
			if (StrUtil.isNotEmpty(userInfo.getPassword())) {
				WHERE("password = #{password}");
			}
			if (StrUtil.isNotEmpty(userInfo.getNickName())) {
				WHERE("nick_name = #{nickName}");
			}
			if (StrUtil.isNotEmpty(userInfo.getRoleInfo())) {
				WHERE("role_info = #{roleInfo}");
			}
			if (StrUtil.isNotEmpty(userInfo.getIdentityCards())) {
				WHERE("identity_cards = #{identityCards}");
			}
			if (null != userInfo.getBirthDay()) {
				WHERE("birth_day = #{birthDay}");
			}
			if (StrUtil.isNotEmpty(userInfo.getTelephone())) {
				WHERE("telephone = #{telephone}");
			}
			if (StrUtil.isNotEmpty(userInfo.getMailbox())) {
				WHERE("mailbox = #{mailbox}");
			}
			if (null != userInfo.getGender()) {
				WHERE("gender = #{gender}");
			}
			if (StrUtil.isNotEmpty(userInfo.getLicensePlateNumber())) {
				WHERE("license_plate_number = #{licensePlateNumber}");
			}
		}}.toString();
		System.out.println(sql);
		return sql;
	}
	
	public String deleteUser(UserInfo userInfo) {
		String sql = new SQL() {{
			DELETE_FROM("user_info");
			WHERE("primary_key = #{primaryKey}");
		}}.toString();
		System.out.println(sql);
		return sql;
	}
	
	public String login(UserInfo userInfo) {
		String sql =  new SQL() {{
			SELECT("primary_key AS primaryKey");
			SELECT("user_name AS userName");
			SELECT("password AS password");
			SELECT("nick_name AS nickName");
			SELECT("role_info AS roleInfo");
			SELECT("identity_cards AS identityCards");
			SELECT("birth_day AS birthDay");
			SELECT("telephone AS telephone");
			SELECT("mailbox AS mailbox");
			SELECT("gender AS gender");
			SELECT("license_plate_number AS licensePlateNumber");
			FROM("user_info");
			WHERE("user_name = #{userName}");
			WHERE("password = #{password}");
		}}.toString();
		System.out.println(sql);
		return sql;
	}
	
	public String register(UserInfo userInfo) {
		String sql = new SQL() {{
			INSERT_INTO("user_info");
			VALUES("primary_key", "#{primaryKey}");
			if (StrUtil.isNotEmpty(userInfo.getUserName())) {
				VALUES("user_name", "#{userName}");
			}
			if (StrUtil.isNotEmpty(userInfo.getPassword())) {
				VALUES("password", "#{password}");
			}
			if (StrUtil.isNotEmpty(userInfo.getNickName())) {
				VALUES("nick_name", "#{nickName}");
			}
			if (StrUtil.isNotEmpty(userInfo.getRoleInfo())) {
				VALUES("role_info", "#{roleInfo}");
			}
			if (StrUtil.isNotEmpty(userInfo.getIdentityCards())) {
				VALUES("identity_cards", "#{identityCards}");
			}
			if (null != userInfo.getBirthDay()) {
				VALUES("birth_day", "#{birthDay}");
			}
			if (StrUtil.isNotEmpty(userInfo.getTelephone())) {
				VALUES("telephone", "#{telephone}");
			}
			if (StrUtil.isNotEmpty(userInfo.getMailbox())) {
				VALUES("mailbox", "#{mailbox}");
			}
			if (null != userInfo.getGender()) {
				VALUES("gender", "#{gender}");
			}
			if (StrUtil.isNotEmpty(userInfo.getLicensePlateNumber())) {
				VALUES("license_plate_number", "#{licensePlateNumber}");
			}
		}}.toString();
		System.out.println(sql);
		return sql;
	}
	
	public String selectUsers(UserInfo userInfo) {
		String sql = new SQL() {{
			SELECT("primary_key AS primaryKey");
			SELECT("user_name AS userName");
			SELECT("password AS password");
			SELECT("nick_name AS nickName");
			SELECT("role_info AS roleInfo");
			SELECT("identity_cards AS identityCards");
			SELECT("birth_day AS birthDay");
			SELECT("telephone AS telephone");
			SELECT("mailbox AS mailbox");
			SELECT("gender AS gender");
			SELECT("license_plate_number AS licensePlateNumber");
			FROM("user_info");
			if (StrUtil.isNotEmpty(userInfo.getUserName())) {
				WHERE("user_name = #{userName}");
			}
			if (StrUtil.isNotEmpty(userInfo.getPassword())) {
				WHERE("password = #{password}");
			}
			if (StrUtil.isNotEmpty(userInfo.getNickName())) {
				WHERE("nick_name = #{nickName}");
			}
			if (StrUtil.isNotEmpty(userInfo.getRoleInfo())) {
				WHERE("role_info = #{roleInfo}");
			}
			if (StrUtil.isNotEmpty(userInfo.getIdentityCards())) {
				WHERE("identity_cards = #{identityCards}");
			}
			if (null != userInfo.getBirthDay()) {
				WHERE("birth_day = #{birthDay}");
			}
			if (StrUtil.isNotEmpty(userInfo.getTelephone())) {
				WHERE("telephone = #{telephone}");
			}
			if (StrUtil.isNotEmpty(userInfo.getMailbox())) {
				WHERE("mailbox = #{mailbox}");
			}
			if (null != userInfo.getGender()) {
				WHERE("gender = #{gender}");
			}
			if (StrUtil.isNotEmpty(userInfo.getLicensePlateNumber())) {
				WHERE("license_plate_number = #{licensePlateNumber}");
			}
		}}.toString();
		System.out.println(sql);
		return sql;
	}
	public String selectUsersJoin(UserInfo userInfo) {
		String sql =  new SQL() {{
			SELECT("user_info.primary_key AS primaryKey");
			SELECT("user_info.user_name AS userName");
			SELECT("user_info.password AS password");
			SELECT("user_info.nick_name AS nickName");
			SELECT("user_info.identity_cards AS identityCards");
			SELECT("user_info.birth_day AS birthDay");
			SELECT("user_info.telephone AS telephone");
			SELECT("user_info.mailbox AS mailbox");
			SELECT("user_info.gender AS gender");
			SELECT("user_info.license_plate_number AS licensePlateNumber");
			SELECT("role_info.display_value AS roleInfo");
			FROM("user_info");
			LEFT_OUTER_JOIN("role_info ON user_info.role_info = role_info.primary_key");
			if (StrUtil.isNotEmpty(userInfo.getUserName())) {
				WHERE("user_info.user_name = #{userName}");
			}
			if (StrUtil.isNotEmpty(userInfo.getPassword())) {
				WHERE("user_info.password = #{password}");
			}
			if (StrUtil.isNotEmpty(userInfo.getNickName())) {
				WHERE("user_info.nick_name = #{nickName}");
			}
			if (StrUtil.isNotEmpty(userInfo.getRoleInfo())) {
				WHERE("user_info.role_info = #{roleInfo}");
			}
			if (StrUtil.isNotEmpty(userInfo.getIdentityCards())) {
				WHERE("user_info.identity_cards = #{identityCards}");
			}
			if (null != userInfo.getBirthDay()) {
				WHERE("user_info.birth_day = #{birthDay}");
			}
			if (StrUtil.isNotEmpty(userInfo.getTelephone())) {
				WHERE("user_info.telephone = #{telephone}");
			}
			if (StrUtil.isNotEmpty(userInfo.getMailbox())) {
				WHERE("user_info.mailbox = #{mailbox}");
			}
			if (null != userInfo.getGender()) {
				WHERE("user_info.gender = #{gender}");
			}
			if (StrUtil.isNotEmpty(userInfo.getLicensePlateNumber())) {
				WHERE("user_info.license_plate_number = #{licensePlateNumber}");
			}
		}}.toString();
		sql.concat(" limit "+(userInfo.getPage()*userInfo.getLimit())+","+userInfo.getLimit());
		System.out.println(sql);
		return sql;
	}
	
	public String selectUser(UserInfo userInfo) {
		String sql =  new SQL() {{
			SELECT("primary_key AS primaryKey");
			SELECT("user_name AS userName");
			SELECT("password AS password");
			SELECT("nick_name AS nickName");
			SELECT("role_info AS roleInfo");
			SELECT("identity_cards AS identityCards");
			SELECT("birth_day AS birthDay");
			SELECT("telephone AS telephone");
			SELECT("mailbox AS mailbox");
			SELECT("gender AS gender");
			SELECT("license_plate_number AS licensePlateNumber");
			FROM("user_info");
			WHERE("primary_key = #{primaryKey}");
		}}.toString();
		System.out.println(sql);
		return sql;
	}
	
	public String updateSelectiveUser(UserInfo userInfo) {
		String sql = new SQL() {{
			UPDATE("user_info");
			if (StrUtil.isNotEmpty(userInfo.getUserName())) {
				SET("user_name = #{userName}");
			}
			if (StrUtil.isNotEmpty(userInfo.getPassword())) {
				SET("password = #{password}");
			}
			if (StrUtil.isNotEmpty(userInfo.getNickName())) {
				SET("nick_name = #{nickName}");
			}
			if (StrUtil.isNotEmpty(userInfo.getRoleInfo())) {
				SET("role_info = #{roleInfo}");
			}
			if (StrUtil.isNotEmpty(userInfo.getIdentityCards())) {
				SET("identity_cards = #{identityCards}");
			}
			if (null != userInfo.getBirthDay()) {
				SET("birth_day = #{birthDay}");
			}
			if (StrUtil.isNotEmpty(userInfo.getTelephone())) {
				SET("telephone = #{telephone}");
			}
			if (StrUtil.isNotEmpty(userInfo.getMailbox())) {
				SET("mailbox = #{mailbox}");
			}
			if (null != userInfo.getGender()) {
				SET("gender = #{gender}");
			}
			if (StrUtil.isNotEmpty(userInfo.getLicensePlateNumber())) {
				SET("license_plate_number = #{licensePlateNumber}");
			}
			WHERE("primary_key = #{primaryKey}");
		}}.toString();
		System.out.println(sql);
		return sql;
	}
	
	public String selectRoles(RoleInfo roleInfo) {
		String sql = new SQL() {{
			SELECT("primary_key AS primaryKey");
			SELECT("display_value AS displayValue");
			FROM("role_info");
			if (StrUtil.isNotEmpty(roleInfo.getDisplayValue())) {
				WHERE("display_value = #{displayValue}");
			}
		}}.toString();
		System.out.println(sql);
		return sql;
	}
	
	public String selectMenus(MenuInfo menuInfo) {
		String sql = new SQL() {{
			SELECT("primary_key AS primaryKey");
			SELECT("menu_name AS menuName");
			SELECT("icon AS icon");
			SELECT("jump AS jump");
			SELECT("role_info AS roleInfo");
			FROM("menu_info");
			if (StrUtil.isNotEmpty(menuInfo.getMenuName())) {
				WHERE("menu_name = #{menuName}");
			}
			if (StrUtil.isNotEmpty(menuInfo.getIcon())) {
				WHERE("icon = #{icon}");
			}
			if (StrUtil.isNotEmpty(menuInfo.getJump())) {
				WHERE("jump = #{jump}");
			}
			if (StrUtil.isNotEmpty(menuInfo.getRoleInfo())) {
				WHERE("role_info = #{roleInfo}");
			}
		}}.toString();
		System.out.println(sql);
		return sql;
	}
	
}
