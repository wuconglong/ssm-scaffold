package ${groupId}.service.impl;

import java.util.List;
import java.util.UUID;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import ${groupId}.dao.IndexMapper;
import ${groupId}.pojo.MenuInfo;
import ${groupId}.pojo.RoleInfo;
import ${groupId}.pojo.UserInfo;
import ${groupId}.service.IndexService;

@Service
public class IndexServiceImpl implements IndexService {
	
	@Autowired
	private IndexMapper indexMapper;
	
	@Override
	public List<UserInfo> selectUsers(UserInfo userInfo) {
		return indexMapper.selectUsers(userInfo);
	}
	
	@Override
	public UserInfo login(UserInfo userInfo) {
		return indexMapper.login(userInfo);
	}

	@Override
	public int register(UserInfo userInfo) {
		UserInfo compositeKeys = new UserInfo();
		compositeKeys.setUserName(userInfo.getUserName());
		if (indexMapper.countUsers(compositeKeys) > 0L) {
			return 0;
		}
		userInfo.setPrimaryKey(UUID.randomUUID().toString());
		return indexMapper.register(userInfo);
	}

	@Override
	public List<MenuInfo> selectMenus(MenuInfo menuInfo) {
		return indexMapper.selectMenus(menuInfo);
	}

	@Override
	public List<RoleInfo> selectRoles(RoleInfo roleInfo) {
		return indexMapper.selectRoles(roleInfo);
	}

	@Override
	public int updateSelectiveUser(UserInfo userInfo) {
		UserInfo compositeKeys = new UserInfo();
		compositeKeys.setUserName(userInfo.getUserName());
		compositeKeys.setPrimaryKey(userInfo.getPrimaryKey());
		if (indexMapper.countUsers(compositeKeys) > 0L) {
			return 0;
		}
		return indexMapper.updateSelectiveUser(userInfo);
	}

	@Override
	public int deleteUser(UserInfo userInfo) {
		return indexMapper.deleteUser(userInfo);
	}

	@Override
	public List<UserInfo> selectUsersJoin(UserInfo userInfo) {
		return indexMapper.selectUsersJoin(userInfo);
	}

	@Override
	public UserInfo selectUser(UserInfo userInfo) {
		return indexMapper.selectUser(userInfo);
	}

	@Override
	public long countUsers(UserInfo userInfo) {
		return indexMapper.countUsers(userInfo);
	}



}
