package ${groupId}.service;

import java.util.List;

import ${groupId}.pojo.MenuInfo;
import ${groupId}.pojo.RoleInfo;
import ${groupId}.pojo.UserInfo;

public interface IndexService {
	
	/**
	 * 登陆接口
	 * 根据用户名与密码为查询条件，查询用户列表。
	 * @param userInfo 携带用户名与密码的用户信息
	 * @return 用户信息
	 */
	public UserInfo login(UserInfo userInfo);
	
	/**
	 * 单表查询用户列表接口
	 * @param userInfo 携带用户信息
	 * @return 用户列表
	 */
	public List<UserInfo> selectUsers(UserInfo userInfo);
	
	/**
	 * 注册用户接口
	 * 首先，设置主键与默认角色
	 * 其次，查询当前用户民有无注册过，如果有直接返回0
	 * 最后，调用持久层新增指定字段的方法，并返回结果
	 * @param userInfo 需要注册的用户信息
	 * @return 是否注册成功
	 */
	public int register(UserInfo userInfo);
	
	/**
	 * 以角色查询所有菜单的接口，设置参数中的角色为查询条件，调用持久层以条件查询菜单列表的接口，并获取菜单列表。
	 * @param menuInfo 携带角色的菜单信息
	 * @return 返回菜单列表。
	 */
	public List<MenuInfo> selectMenus(MenuInfo menuInfo);
	
	/**
	 * 查询所有角色的接口，直接调用持久层查询所有角色的接口，并返回结果。
	 * @return 返回有所的角色信息。
	 */
	public List<RoleInfo> selectRoles(RoleInfo roleInfo);
	
	/**
	 * 以主键修改指定字段的接口，根据参数，以等于用户名和不等于主键两个条件，查询记录数。
	 * @param userInfo 需要修改的用户信息
	 * @return 如果记录数大于0，则返回0；否则即调用持久层以主键修改用户指定字段的接口，并返回结果。
	 */
	public int updateSelectiveUser(UserInfo userInfo);
	
	/**
	 * 删除用户的接口
	 * @param userInfo 携带用户主键
	 * @return 返回受影响行数。
	 */
	public int deleteUser(UserInfo userInfo);
	
	/**
	 * 联表查询用户列表接口
	 * 先判断用户名、昵称、性别三个字段是否为空，在参数作为查询条件，查询用户列表
	 * @param userInfo 携带用户名、昵称、性别的用户信息
	 * @return 用户列表
	 */
	public List<UserInfo> selectUsersJoin(UserInfo userInfo);
	
	/**
	 * 以主键查询单个用户信息的接口，直接根据参数调用持久层以主键查询单个用户信息的接口，并返回结果。
	 * @param userInfo 携带主键的用户信息
	 * @return 返回单个用户信息。
	 */
	public UserInfo selectUser(UserInfo userInfo);
	
	/**
	 * 计数接口
	 * 根据传递的参数，进行计数。
	 * @param userInfo 携带主键的用户信息
	 * @return 记录数
	 */
	public long countUsers(UserInfo userInfo);

}
