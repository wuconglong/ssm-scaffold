package ${groupId}.util;

import java.util.Set;

public class TableInfo {
	public String tableName;//表名
	public String className;//类名
	public int tableType;//1代表是数据字典表，2代表业务表
	public String leftJoinTableName;//需要外连接的表名
	public Set<String> imports;//需要导入的包
	public String notes;//注释
}