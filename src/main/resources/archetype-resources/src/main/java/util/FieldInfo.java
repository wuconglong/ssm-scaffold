package ${groupId}.util;

public class FieldInfo {
    public String dbName;//表字段名
    public String dbType;//表字段类型
    public String javaName;//java属性名
    public String javaType;//java属性类型
    public int htmlFormType;//页面表单类型，1代表文本输入框，2代表时间输入框
    public String length;//表字段长度，时间日期类型为格式
    public String foreignKeyTableName;//外键表名
    public String jsRegular;//js正则验证方式
    public int listParam;//列表页面的查询参数，0代表不是参数，1代表=，2代表like
    public boolean textField;//是否为文本字段，true为是，false为不是
    public boolean orderType;//排序类型，true为倒序，false为正序
    public boolean compositeKeys;//是否为联合主键，true代表是，false代表不是
    public String notes;//表字段注释
    public String getMethod;//表字段注释
    public String setMethod;//表字段注释
	public String getDbName() {
		return dbName;
	}
	public void setDbName(String dbName) {
		this.dbName = dbName;
	}
	public String getDbType() {
		return dbType;
	}
	public void setDbType(String dbType) {
		this.dbType = dbType;
	}
	public String getJavaName() {
		return javaName;
	}
	public void setJavaName(String javaName) {
		this.javaName = javaName;
	}
	public String getJavaType() {
		return javaType;
	}
	public void setJavaType(String javaType) {
		this.javaType = javaType;
	}
	public int getHtmlFormType() {
		return htmlFormType;
	}
	public void setHtmlFormType(int htmlFormType) {
		this.htmlFormType = htmlFormType;
	}
	public String getLength() {
		return length;
	}
	public void setLength(String length) {
		this.length = length;
	}
	public String getForeignKeyTableName() {
		return foreignKeyTableName;
	}
	public void setForeignKeyTableName(String foreignKeyTableName) {
		this.foreignKeyTableName = foreignKeyTableName;
	}
	public String getJsRegular() {
		return jsRegular;
	}
	public void setJsRegular(String jsRegular) {
		this.jsRegular = jsRegular;
	}
	public int getListParam() {
		return listParam;
	}
	public void setListParam(int listParam) {
		this.listParam = listParam;
	}
	public boolean isTextField() {
		return textField;
	}
	public void setTextField(boolean textField) {
		this.textField = textField;
	}
	public boolean isOrderType() {
		return orderType;
	}
	public void setOrderType(boolean orderType) {
		this.orderType = orderType;
	}
	public boolean isCompositeKeys() {
		return compositeKeys;
	}
	public void setCompositeKeys(boolean compositeKeys) {
		this.compositeKeys = compositeKeys;
	}
	public String getNotes() {
		return notes;
	}
	public void setNotes(String notes) {
		this.notes = notes;
	}
	public String getGetMethod() {
		return getMethod;
	}
	public void setGetMethod(String getMethod) {
		this.getMethod = getMethod;
	}
	public String getSetMethod() {
		return setMethod;
	}
	public void setSetMethod(String setMethod) {
		this.setMethod = setMethod;
	}
    
}