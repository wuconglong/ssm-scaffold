package ${groupId}.util;

import java.io.File;
import java.util.ArrayList;
import java.util.List;

import cn.hutool.core.util.StrUtil;

public class StringUtil {
	
	public static String tableNameConvert(String tableName) {
		String[] tables = tableName.split("_");
		String result = "";
		if (tables.length > 0) {
			for (int i = 0; i < tables.length; i++) {
				result += firstLetterUppercase(tables[i]);
			}
		}
		return result;
	}
	
	public static String fieldNameConvert(String filedName) {
		String[] fileds = filedName.split("_");
		String result = fileds[0];
		if (fileds.length > 1) {
			for (int i = 1; i < fileds.length; i++) {
				result += firstLetterUppercase(fileds[i]);
			}
		}
		return result;
	}
	
	/**
	 * 使用分隔符拆分，获得一个集合对象
	 * @param resourse指定的字符串
	 * @param separator分隔符
	 * @return
	 */
	public static List<String> split(String resourse,String separator){
		List<String> ls = new ArrayList<String>();
		int index = 0;
		while ((index = resourse.indexOf(separator)) > 0) {
			ls.add(resourse.substring(0, index));
			resourse = resourse.substring(index+1);
			index = 0;
		}
		ls.add(resourse);
		return ls;
	}
	
	/**
	 * 获取指定的字符串在分隔符后面的字符串，如果分隔符为空，那么默认使用路径分隔符
	 * @param resourse指定的字符串
	 * @param separator分隔符
	 * @return
	 */
	public static String getLast(String resourse,String separator) {
		if (separator == null) {
			return resourse.substring(resourse.lastIndexOf(System.getProperty("file.separator"))+1);
		} else {
			return resourse.substring(resourse.lastIndexOf(separator)+1);
		}
	}
	
	/**
	 * 获取tomcat路径下的文件路径，此方法需要启动tomcat才可使用不然会NPE
	 * @param strings各级文件名，可为空
	 * @return
	 */
	public static String concatCatalinaPath(String... strings) {
		int length = strings.length;
		StringBuilder sb = new StringBuilder(System.getProperty("catalina.home"));
		sb.append(System.getProperty("file.separator")).append("webapps");
		for (int i = 0; i < length; i++) {
			sb.append(System.getProperty("file.separator")).append(strings[i]);
		}
		sb.append(System.getProperty("file.separator"));
		return sb.toString();
	}
	
	public static String concatPath(String... strings) {
		StringBuilder sb = new StringBuilder();
		for (int i = 0; i < strings.length; i++) {
			sb.append(strings[i]);
			sb.append(File.separator);
		}
		return sb.toString();
	}
	
	public static String firstLetterLowercase(String word) {
		return word.substring(0, 1).toLowerCase().concat(word.substring(1, word.length()));
	}
	public static String firstLetterUppercase(String word) {
		return word.substring(0, 1).toUpperCase().concat(word.substring(1, word.length()));
	}
	public static String getMethod(String word) {
		return "get".concat(firstLetterUppercase(word));
	}
	public static String setMethod(String word) {
		return "set".concat(firstLetterUppercase(word));
	}
	
	public static void main(String[] args) {
		System.out.println(StrUtil.format("{}Example example = new {}Example();", "Cars","Cars"));
	}
	
}
