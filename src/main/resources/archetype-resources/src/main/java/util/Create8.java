package ${groupId}.util;

import java.io.File;
import java.io.FileWriter;
import java.util.List;

import org.apache.velocity.Template;
import org.apache.velocity.VelocityContext;
import org.apache.velocity.app.Velocity;

import wcl.util.StringUtil;

public class Create8 {
	
	DBUtil d = new DBUtil();
	List<TableAndFields> list = null;
	
	public void pojo() throws Exception {
		for (int i = 0; i < list.size(); i++) {
			TableAndFields taf = list.get(i);
			VelocityContext context = new VelocityContext();
			context.put("groupId", "${groupId}");
			context.put("className", taf.tableInfo.className);
			context.put("list", taf.fieldInfos);
			context.put("imports", taf.tableInfo.imports);
			File file = new File("./src/main/java/${groupId}/pojo/"+taf.tableInfo.className+".java");
			Template template = Velocity.getTemplate("./src/main/resources/vms/pojo.vm", "UTF-8");
	        FileWriter writer = new FileWriter(file);
	        template.merge(context, writer);
	        writer.close();
		}
	}
	
	public void mapper() throws Exception {
		for (int i = 0; i < list.size(); i++) {
			TableAndFields taf = list.get(i);
			VelocityContext context = new VelocityContext();
			context.put("groupId", "${groupId}");
			context.put("className", taf.tableInfo.className);
			context.put("lowerClassName", StringUtil.firstLetterLowercase(taf.tableInfo.className));
			File file = new File("./src/main/java/${groupId}/dao/"+taf.tableInfo.className+"Mapper.java");
			Template template = Velocity.getTemplate("./src/main/resources/vms/mapper.vm", "UTF-8");
	        FileWriter writer = new FileWriter(file);
	        template.merge(context, writer);
	        writer.close();
		}
	}
	
	public void provider() throws Exception {
		for (int i = 0; i < list.size(); i++) {
			TableAndFields taf = list.get(i);
			VelocityContext context = new VelocityContext();
			context.put("groupId", "${groupId}");
			context.put("className", taf.tableInfo.className);
			context.put("tableName", taf.tableInfo.tableName);
			context.put("list", taf.fieldInfos);
			context.put("leftJoinTableName", taf.tableInfo.leftJoinTableName);
			context.put("lowerClassName", StringUtil.firstLetterLowercase(taf.tableInfo.className));
			File file = new File("./src/main/java/${groupId}/dao/"+taf.tableInfo.className+"Provider.java");
			Template template = Velocity.getTemplate("./src/main/resources/vms/provider.vm", "UTF-8");
	        FileWriter writer = new FileWriter(file);
	        template.merge(context, writer);
	        writer.close();
		}
	}
	
	public void service() throws Exception {
		for (int i = 0; i < list.size(); i++) {
			TableAndFields taf = list.get(i);
			VelocityContext context = new VelocityContext();
			context.put("groupId", "${groupId}");
			context.put("className", taf.tableInfo.className);
			context.put("lowerClassName", StringUtil.firstLetterLowercase(taf.tableInfo.className));
			File file = new File("./src/main/java/${groupId}/service/"+taf.tableInfo.className+"Service.java");
			Template template = Velocity.getTemplate("./src/main/resources/vms/service.vm", "UTF-8");
	        FileWriter writer = new FileWriter(file);
	        template.merge(context, writer);
	        writer.close();
		}
	}
	
	public void impl() throws Exception {
		for (int i = 0; i < list.size(); i++) {
			TableAndFields taf = list.get(i);
			VelocityContext context = new VelocityContext();
			context.put("groupId", "${groupId}");
			context.put("className", taf.tableInfo.className);
			context.put("list", taf.fieldInfos);
			context.put("lowerClassName", StringUtil.firstLetterLowercase(taf.tableInfo.className));
			File file = new File("./src/main/java/${groupId}/service/impl/"+taf.tableInfo.className+"ServiceImpl.java");
			Template template = Velocity.getTemplate("./src/main/resources/vms/impl.vm", "UTF-8");
	        FileWriter writer = new FileWriter(file);
	        template.merge(context, writer);
	        writer.close();
		}
	}
	
	public void controller() throws Exception {
		for (int i = 0; i < list.size(); i++) {
			TableAndFields taf = list.get(i);
			VelocityContext context = new VelocityContext();
			context.put("groupId", "${groupId}");
			context.put("className", taf.tableInfo.className);
			context.put("lowerClassName", StringUtil.firstLetterLowercase(taf.tableInfo.className));
			File file = new File("./src/main/java/${groupId}/controller/"+taf.tableInfo.className+"Controller.java");
			Template template = Velocity.getTemplate("./src/main/resources/vms/controller.vm", "UTF-8");
	        FileWriter writer = new FileWriter(file);
	        template.merge(context, writer);
	        writer.close();
		}
	}
	
	public void list() throws Exception {
		for (int i = 0; i < list.size(); i++) {
			TableAndFields taf = list.get(i);
			VelocityContext context = new VelocityContext();
			context.put("artifactId", "create5html");
			context.put("className", taf.tableInfo.className);
			context.put("list", taf.fieldInfos);
			context.put("lowerClassName", StringUtil.firstLetterLowercase(taf.tableInfo.className));
			File file = new File("./src/main/webapp/WEB-INF/jsp/"+StringUtil.firstLetterLowercase(taf.tableInfo.className)+"List.jsp");
			Template template = Velocity.getTemplate("./src/main/resources/vms/list.vm", "UTF-8");
	        FileWriter writer = new FileWriter(file);
	        template.merge(context, writer);
	        writer.close();
		}
	}
	
	public void form() throws Exception {
		for (int i = 0; i < list.size(); i++) {
			TableAndFields taf = list.get(i);
			VelocityContext context = new VelocityContext();
			context.put("artifactId", "create5html");
			context.put("className", taf.tableInfo.className);
			context.put("list", taf.fieldInfos);
			context.put("lowerClassName", StringUtil.firstLetterLowercase(taf.tableInfo.className));
			File file = new File("./src/main/webapp/WEB-INF/jsp/"+StringUtil.firstLetterLowercase(taf.tableInfo.className)+"Form.jsp");
			Template template = Velocity.getTemplate("./src/main/resources/vms/form.vm", "UTF-8");
	        FileWriter writer = new FileWriter(file);
	        template.merge(context, writer);
	        writer.close();
		}
	}
	
	public Create8() {
		try {
			list = d.create();
		} catch (Exception e) {
			e.printStackTrace();
		}
	}
	
	public static void main(String[] args) throws Exception {
		new Create8().create8();
	}
	
	public void create8() throws Exception {
		pojo();
		mapper();
		provider();
		service();
		impl();
		controller();
		list();
		form();
	}

}
